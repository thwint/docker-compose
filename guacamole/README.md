# Guacamole #
This describes how to quickly start an Apache Guacamole instance with MariaDB (MySQL) backend using docker-compose.

## About Guacamole ##
Apache Guacamole is a clientless remote desktop gateway. It supports standard protocols like VNC, RDP, and SSH.

For further details go checkout the [Guacamole homepage](https://guacamole.apache.org/)

## Quick start ##
Clone the git repository and start your Guacamole instance:

~~~bash
git clone https://bitbucket.org/thwint/docker-compose.git
cd guacamole
./prepare.sh
docker-compose up -d
~~~

## Details ##
This docker-compose file creates a bridged docker network named 'guacamole' and the containers 'guacamole.guacd', 'guacamole.guacamole' and 'guacamole.db'

### guacd ###
Provides the guacd daemon, built from the released guacamole-server source with support for VNC, RDP, SSH, telnet, and Kubernetes.

~~~yaml
guacamole.guacd:
  image: guacamole/guacd
  container_name: guacamole.guacd
  hostname: guacd
  networks:
    - guacamole
~~~

### guacamole ###
Provides the Guacamole web application running within Tomcat 8 with support for WebSocket. The configuration necessary to connect to guacd, MySQL, PostgreSQL, LDAP, etc. will be generated automatically when the image starts based on Docker links or environment variables.

Run guacamole and make it accessible on port 8080

~~~yaml
guacamole.guacamole:
  image: guacamole/guacamole
  container_name: guacamole.guacamole
  hostname: guacamole
  depends_on:
    - guacamole.guacd
    - guacamole.db
  environment:
    GUACD_HOSTNAME: guacd
    GUACD_PORT: 4822
    MYSQL_HOSTNAME: mariadb
    MYSQL_DATABASE: guacd
    MYSQL_USER: guacd
    MYSQL_PASSWORD: password
  ports:
    - 8080:8080
  networks:
    - guacamole
~~~

### MariaDB ###
Provides the database that Guacamole will use for authentication and storage of connection configuration data.

To initialize the database ensure to add all required sql files into the init folder. If you don't know what to do here just run the following command:

~~~bash
docker run --rm guacamole/guacamole /opt/guacamole/bin/initdb.sh --mysql > ./init/00_initdb.sql
~~~

~~~yaml
guacamole.db:
  image: mariadb
  container_name: guacamole.db
  hostname: mariadb
  environment:
    MYSQL_ROOT_PASSWORD: rootpassword
    MYSQL_DATABASE: guacd
    MYSQL_USER: guacd
    MYSQL_PASSWORD: password
  volumes:
    - ./init:/docker-entrypoint-initdb.d:ro
    - ./data:/var/lib/mysql:rw
  networks:
      - guacamole
~~~

### prepare.sh ###
`prepare.sh`is a shell script to automatically prepare the directory structure and create the initdb.sql file.

It creates the following files and directories:

* init
* init/00_initdb.sql
* data
