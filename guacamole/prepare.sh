#!/bin/bash

mkdir -p init data
docker run --rm guacamole/guacamole /opt/guacamole/bin/initdb.sh --mysql > ./init/00_initdb.sql
